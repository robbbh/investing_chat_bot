#!usr/bin/python
# -*- coding: utf-8 -*-

from telepot.namedtuple import InlineKeyboardButton, InlineKeyboardMarkup
from telepot.exception import *
import telepot
import time
from coinmarketcap import Market
import datetime



class telegram_handle:

    def __init__(self):
        # bot API key
        self.bot = telepot.Bot('545907669:AAHeeFOG4xeGbVjQ2zXWJv4ZD2_gzQGcKL4')
        self.bot.message_loop(self.handle)
        self.chat_id = None
        


    def handle(self, msg):
        print msg 
        print ""
        try:
            self.userName = msg["from"]["username"]
        except:
            self.firstName = msg["from"]["first_name"]
            self.lastName = msg["from"]["last_name"]
            self.userName = "{0} {1}".format(self.firstName, self.lastName)

        print "User: {0}".format(self.userName)

        try:
            if str(msg["message"]["from"]["first_name"]) == "investing_chat_bot":
                self.chat_id = msg['message']['chat']['id']
                print self.chat_id
                prices = crypto_prices(msg)
                message = prices.reply_to_chat()
                self.message_chat_with_data(message)
                return
        except Exception, err:
            try:
                print "Error: {0}".format(err)
                print traceback.print_tb
                # raise TypeError("Again !?!")
            except:
                pass

        try:
            self.chat_id = msg['chat']['id']
            print self.chat_id
            self.check_command(msg)
        except:
            pass


    def message_chat_with_data(self, message):
        print self.chat_id
        try:
            self.bot.sendMessage(self.chat_id, message, parse_mode='Markdown')
        except Exception, err:
            print err
        return


    def help_info_reply(self):
        help_message = "*Currently supported commands;*\n" \
        "*/price* - Prices for Bitcoin, Etherium, Ripple and Litecoin\n" \
        "*/change* - Change in prices over 24 hours"
        self.bot.sendMessage(self.chat_id, help_message, parse_mode='Markdown')
        return


    def change_reply(self):
        command = " change"
        self.crypto_custom_keyboard_launch(command)
        return


    def check_command(self, msg):
        self.command = str(msg["text"]) 
        print "User {0} sent the message: {1}".format(self.userName, self.command)
        if self.command == "/prices" or self.command == "/price":
            command = " price"
            self.crypto_custom_keyboard_launch(command)
        elif self.command == "/help":
            self.help_info_reply()
        elif self.command == "/change":
            self.change_reply()


        else:
            self.bot.sendMessage(self.chat_id,"Valid command pls.")
            return


    def crypto_custom_keyboard_launch(self, command):


        my_inline_keyboard = [[
            InlineKeyboardButton(text='Bitcoin', callback_data='bitcoin' + command),
            InlineKeyboardButton(text='Ethereum', callback_data='ethereum' + command),
        ], [
            InlineKeyboardButton(text='Litecoin', callback_data='litecoin' + command),
            InlineKeyboardButton(text='Ripple', callback_data='ripple' + command),
        ]]

        keyboard = InlineKeyboardMarkup(inline_keyboard=my_inline_keyboard )
        self.bot.sendMessage(self.chat_id, "Choose a crypto:", reply_markup=keyboard)


    def run(self):
        while True:
            time.sleep(1)


class crypto_prices:

    def __init__(self, msg):
        self.msg = msg
        self.crypto_type = str(self.msg["data"]).split()[0]
        self.crypto_info = str(self.msg["data"]).split()[1]
        # print self.crypto_type
        # self.epoch_time = str(self.msg["last_updated"])        
        self.coinmarketcap = Market()


    def query_coinmarketcap_api(self, coin_type, currency):
        price_currency = currency.lower()
        coinmarketcap_data = self.coinmarketcap.ticker(coin_type, limit=3, convert=currency)
        price_to_currency = "{0}".format(str(coinmarketcap_data[0]["price_" + price_currency]))
        coin_name = str(coinmarketcap_data[0]['name'])
        #print price_to_gbp
        update_time = str(coinmarketcap_data[0]['last_updated'])
        percent_change_1h = str(coinmarketcap_data[0]['percent_change_1h'])
        percent_change_24h = str(coinmarketcap_data[0]['percent_change_24h'])
        percent_change_7d = str(coinmarketcap_data[0]['percent_change_7d'])
        return (price_to_currency, coin_name, update_time, percent_change_1h,
             percent_change_24h, percent_change_7d)


    def get_price_response(self, usd_data, gbp_data, eur_data):
        try:
            self.time_of_data = self.get_time_of_data(usd_data[2])
        except Exception, err:
            print err
        self.coin_type = usd_data[1] 
        self.usd_price = usd_data[0]
        self.gbp_price = gbp_data[0]
        self.eur_price = eur_data[0]

        message_to_chat = "{0} prices issued at *{1}*;\n"\
        "*{0} (1)* = ${2}\n"\
        "*{0} (1)* = £{3}\n"\
        "*{0} (1)* = €{4}".format(self.coin_type, self.time_of_data,
                                    self.usd_price, self.gbp_price, self.eur_price)
        
        return message_to_chat


    def get_change_response(self, usd_data, gbp_data, eur_data):
        try:
            self.time_of_data = self.get_time_of_data(usd_data[2])
        except Exception, err:
            print err

        self.coin_type = usd_data[1]        
        self.percent_change_1h = usd_data[3]
        self.percent_change_24h = usd_data[4]
        self.percent_change_7d = usd_data[5]

        message_to_chat = "{0} price changes issued at *{1}*;\n"\
        "*1 hour change* = {2}%\n"\
        "*24 hour change* = {3}%\n"\
        "*7 day change* = {4}%".format(self.coin_type, self.time_of_data,
                                    self.percent_change_1h, self.percent_change_24h, 
                                    self.percent_change_7d)

        return message_to_chat


    def get_coin_data(self):


        usd_data = self.query_coinmarketcap_api(self.crypto_type, 'USD')

        gbp_data = self.query_coinmarketcap_api(self.crypto_type, 'GBP')

        eur_data = self.query_coinmarketcap_api(self.crypto_type, 'EUR')


        if self.crypto_info == "price":
            self.message_to_chat = self.get_price_response(usd_data, gbp_data, eur_data)
        elif self.crypto_info == "change":
            self.message_to_chat = self.get_change_response(usd_data, gbp_data, eur_data)

        return self.message_to_chat
        
        
    def get_time_of_data(self, epoch_time):
        self.epoch_time = float(epoch_time)
        # print "Before epoch: {0}".format(self.epoch_time)
        # print type(self.epoch_time)
        self.local_time = datetime.datetime.fromtimestamp(self.epoch_time).strftime('%Y-%m-%d %H:%M:%S')
        # print "After epoch: {0}".format(self.epoch_time)
        # print "local time: {0}".format(self.local_time)
        return self.local_time
        

    def reply_to_chat(self):
        coin_data = self.get_coin_data()
        print coin_data
        return coin_data



        

if __name__ == "__main__":
    execute_bot = telegram_handle()
    execute_bot.run()