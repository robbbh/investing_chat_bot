# README #

### What is this repository for? ###

* Telegram bot for the investing chat to get info on cryptocurrency prices and changes.
* Currently designed to be run on Openshift
* Written in Python 2.7
* Uses telepot and coinmarketcap Python API's
 

### How do I get set up? ###

* Add the user ***@lads_investing_bot*** on telegram
* Run the commands:
```
   /price - Get's current prices of bitcoin, ethereum, litecoin, ripple
   /change - Get's the percentage change for 1 hour, 24 hour and 7 day intervals for bitcoin, ethereum, litecoin and ripple
   /help - Gives info on the commands
```

### Author ###

Robert Harris - harrisrp95@gmail.com
